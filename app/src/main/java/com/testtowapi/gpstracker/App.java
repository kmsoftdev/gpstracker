package com.testtowapi.gpstracker;

import android.app.Application;

import com.testtowapi.gpstracker.di.AppComponent;
import com.testtowapi.gpstracker.di.AppModule;
import com.testtowapi.gpstracker.di.DaggerAppComponent;
import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;

public class App extends Application {
    private AppComponent mAppComponent;
    private RefWatcher mRefWatcher;

    public AppComponent getAppComponent() {
        return this.mAppComponent;
    }

    public RefWatcher getRefWatcher() {
        return this.mRefWatcher;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        this.mRefWatcher = LeakCanary.install(this);
        this.mAppComponent = buildComponent();
    }

    protected AppComponent buildComponent() {
        return DaggerAppComponent.builder().appModule(new AppModule(this)).build();
    }
}
