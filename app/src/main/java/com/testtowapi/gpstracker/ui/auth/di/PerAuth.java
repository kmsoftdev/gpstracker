package com.testtowapi.gpstracker.ui.auth.di;

/**
 * Created by Think on 19/09/2016.
 */

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface PerAuth {
}
