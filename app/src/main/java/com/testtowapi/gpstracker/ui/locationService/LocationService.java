package com.testtowapi.gpstracker.ui.locationService;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.widget.RemoteViews;

import com.testtowapi.gpstracker.App;
import com.testtowapi.gpstracker.R;
import com.testtowapi.gpstracker.ui.locationService.di.LocationServiceModule;

import javax.inject.Inject;

public class LocationService extends Service implements LocationServiceView {
    public static final int NOTIFICATION_COMMAND_STOP = 1;
    public static final int NOTIFICATION_ID = 1345;
    private static final String KEY_START = "START_STOP";
    @Inject LocationServicePresenter mPresenter;
    private int mStartId;

    @Override
    public void onCreate() {
        super.onCreate();
        ((App) getApplication()).getAppComponent().sub(new LocationServiceModule()).inject(this);
        mPresenter.setView(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null && intent.getExtras() != null) {
            if (intent.getExtras().containsKey(KEY_START)) {
                mPresenter.locationServiceStarted(intent.getBooleanExtra(KEY_START, false));
                this.mStartId = startId;
            }
        }
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.setView(null);
        mPresenter.onPause();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * displays notification
     */
    @Override
    public void displaySelf() {
        RemoteViews remoteViews = new RemoteViews(getPackageName(), R.layout.view_notification);
        Intent intent = createIntent(this, false);
        PendingIntent closeCommand = PendingIntent.getService(this, 0, intent, 0);
        remoteViews.setOnClickPendingIntent(R.id.close, closeCommand);
        Notification notification = new NotificationCompat.Builder(getApplicationContext())
                .setContentText(getText(R.string.streaming_location_updates))
                .setSmallIcon(R.drawable.ic_action_location)
                .setCustomContentView(remoteViews)
                .setCustomBigContentView(remoteViews)
                .setOngoing(true)
                .setAutoCancel(true)
                .build();
        startForeground(NOTIFICATION_ID, notification);
    }

    public static Intent createIntent(Context context, boolean start) {
        Intent intent = new Intent(context, LocationService.class);
        intent.putExtra(KEY_START, start);
        return intent;
    }

    @Override
    public void killSelf() {
        stopForeground(true);
        stopSelf();
    }

}
