package com.testtowapi.gpstracker.ui.home.di;

import com.testtowapi.gpstracker.ui.home.HomeActivity;

import dagger.Module;

@Module
public class HomeModule {

    private HomeActivity mInjectee;

    public HomeModule(HomeActivity injectee) {
        this.mInjectee = injectee;
    }

}
