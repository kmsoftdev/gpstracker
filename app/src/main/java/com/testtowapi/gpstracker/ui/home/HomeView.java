package com.testtowapi.gpstracker.ui.home;

import com.testtowapi.gpstracker.ui.BaseView;

public interface HomeView extends BaseView {

    void showLocationServiceStatus(boolean isStarted);
}
