package com.testtowapi.gpstracker.ui.home;

import com.testtowapi.gpstracker.business.Bus;
import com.testtowapi.gpstracker.business.bus.locationServiceStatus.LocationStatusEventContext;
import com.testtowapi.gpstracker.business.location.LocationServiceInteractor;
import com.testtowapi.gpstracker.ui.Presenter;
import com.testtowapi.gpstracker.ui.home.di.PerHome;

import javax.inject.Inject;

import rx.Subscription;

@PerHome
public class HomePresenter extends Presenter<HomeView> implements LocationStatusEventContext {
    private final LocationServiceInteractor mLocationServiceInteractor;
    private final Bus mBus;
    private Subscription mLocationServiceStatus;

    @Inject
    public HomePresenter(LocationServiceInteractor locationServiceInteractor, Bus bus) {

        mLocationServiceInteractor = locationServiceInteractor;
        mBus = bus;
    }

    @Override
    public void onResume() {
        super.onResume();
        mLocationServiceStatus = mBus.subscribeToBusEvents(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mLocationServiceStatus != null && !mLocationServiceStatus.isUnsubscribed()) {
            mLocationServiceStatus.unsubscribe();
        }
    }

    @Override
    public void onCreateView() {
        super.onCreateView();
        mLocationServiceInteractor.startLocationService(true);
    }

    public void onCheckedChanged(boolean start) {
        mLocationServiceInteractor.startLocationService(start);
    }

    @Override
    public void locationServiceStatusChanged(boolean isStarted) {
        mView.showLocationServiceStatus(isStarted);
    }
}