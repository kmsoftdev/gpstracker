package com.testtowapi.gpstracker.ui.locationService;

import android.util.Log;

import com.testtowapi.gpstracker.business.location.LocationReceiver;
import com.testtowapi.gpstracker.ui.Presenter;
import com.testtowapi.gpstracker.ui.locationService.di.PerLocationService;

import javax.inject.Inject;

@PerLocationService
public class LocationServicePresenter extends Presenter<LocationServiceView> {
    private final LocationReceiver mLocationReceiver;

    @Inject
    public LocationServicePresenter(
            LocationReceiver locationReceiver) {

        mLocationReceiver = locationReceiver;
    }

    public void locationServiceStarted(boolean start) {
        if (start) {
            Log.d("kmtag", "started: ");
            mView.displaySelf();
            mLocationReceiver.onResume();
        }
        else {
            Log.d("kmtag", "stopSelf: ");
            mLocationReceiver.onPause();
            mView.killSelf();
        }
    }
}
