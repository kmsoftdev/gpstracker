package com.testtowapi.gpstracker.ui.home.di;

import com.testtowapi.gpstracker.ui.home.HomeActivity;

import dagger.Subcomponent;

@PerHome
@Subcomponent(modules = {HomeModule.class})
public interface HomeComponent {

    void inject(HomeActivity homeActivity);
}


