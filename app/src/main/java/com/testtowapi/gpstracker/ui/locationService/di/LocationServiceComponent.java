package com.testtowapi.gpstracker.ui.locationService.di;

import com.testtowapi.gpstracker.ui.locationService.LocationService;

import dagger.Subcomponent;

@PerLocationService
@Subcomponent(modules = LocationServiceModule.class)
public interface LocationServiceComponent {
    void inject(LocationService injectee);
}


