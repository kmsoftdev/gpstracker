package com.testtowapi.gpstracker.ui.auth;

import android.util.Log;

import com.testtowapi.gpstracker.business.auth.AuthController;
import com.testtowapi.gpstracker.model.auth.Credentials;
import com.testtowapi.gpstracker.ui.Presenter;
import com.testtowapi.gpstracker.ui.auth.di.PerAuth;

import javax.inject.Inject;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

@PerAuth
public class AuthPresenter extends Presenter<AuthView> {
    private final AuthController mAuthController;
    private Subscription mSavedUserSubscription;
    private Subscription mLogInSubscription;

    @Inject
    public AuthPresenter(AuthController authController) {
        mAuthController = authController;
    }

    @Override
    public void onCreateView() {
        super.onCreateView();
        restoreSavedCredentials();

    }

    private void restoreSavedCredentials() {
        this.mSavedUserSubscription = mAuthController
                .getSavedCredentials()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(savedCredentials -> {
                    if (mView != null) {
                        mView.setSavedCredentials(savedCredentials);
                    }
                }, throwable -> {
                    Log.d("kmtag", "throwable: " + throwable.getLocalizedMessage());
                });
    }

    /**
     * login button pressed
     */
    public void logIn(Credentials credentials) {
        if (!validate(credentials)) return;

        this.mLogInSubscription = mAuthController.logIn(credentials)
                                                 .subscribeOn(Schedulers.io())
                                                 .observeOn(AndroidSchedulers.mainThread())
                                                 .subscribe(loggedInSuccessfully -> {
                                                     if (loggedInSuccessfully) {
                                                         mView.logInSuccess();
                                                     }
                                                     else {
                                                         mView.showInfo("There's been a problem signing you in");
                                                     }
                                                 }, throwable -> {
                                                     mView.showInfo(throwable.getLocalizedMessage());
                                                 });

    }

    private boolean validate(Credentials credentials) {
        boolean validated = true;
        if (emptyOrNull(credentials.getUsername())) {
            mView.showEmptyUsernameError(true);
            validated = false;
        }
        else {
            mView.showEmptyUsernameError(false);
        }
        if (emptyOrNull(credentials.getPassword())) {
            mView.showEmptyPasswordError(true);
            validated = false;
        }
        else {
            mView.showEmptyPasswordError(false);

        }
        return validated;
    }

    private boolean emptyOrNull(String s) {
        return s == null || s.isEmpty();
    }

    /**
     * cancel button pressed
     */
    public void cancel() {
        if (mView != null) {
            mView.finish();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mSavedUserSubscription != null && !mSavedUserSubscription.isUnsubscribed()) {
            mSavedUserSubscription.unsubscribe();
        }
        if (mLogInSubscription != null && !mLogInSubscription.isUnsubscribed()) {
            mLogInSubscription.unsubscribe();
        }
    }
}
