package com.testtowapi.gpstracker.ui.auth.di;

import com.testtowapi.gpstracker.ui.auth.AuthActivity;
import com.testtowapi.gpstracker.ui.auth.AuthDialog;

import dagger.Module;

@Module
public class AuthModule {

    private AuthDialog mAuthDialog;
    private AuthActivity mInjectee;

    public AuthModule(AuthActivity injectee) {
        this.mInjectee = injectee;
    }

    public AuthModule(AuthDialog authDialog) {
        this.mAuthDialog = authDialog;
    }
}
