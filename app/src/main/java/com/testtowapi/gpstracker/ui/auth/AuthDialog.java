package com.testtowapi.gpstracker.ui.auth;

import android.app.DialogFragment;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.testtowapi.gpstracker.App;
import com.testtowapi.gpstracker.R;
import com.testtowapi.gpstracker.databinding.ContentAuthBinding;
import com.testtowapi.gpstracker.model.auth.Credentials;
import com.testtowapi.gpstracker.ui.auth.di.AuthModule;

import javax.inject.Inject;

/**
 * Created by Think on 20/09/2016.
 */

public class AuthDialog extends DialogFragment implements AuthView {
    @Inject AuthPresenter mPresenter;
    private ContentAuthBinding mBinding;
    private HomeActivityCallback mActivityCallback;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((App) getActivity().getApplication()).getAppComponent().sub(new AuthModule(this)).inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.content_auth, container, false);
        mBinding.setEventHandler(mPresenter);
        mPresenter.setView(this);
        mPresenter.onCreateView();
        setCancelable(false);
        return mBinding.getRoot();
    }

    @Override
    public void setSavedCredentials(Credentials credentials) {
        mBinding.setCredentials(credentials);
    }

    @Override
    public void finish() {

        mActivityCallback.cancel();
    }

    @Override
    public void logInSuccess() {
        Toast.makeText(getActivity(), "Login success", Toast.LENGTH_SHORT).show();
        dismiss();
        mActivityCallback.loginSuccess();

    }

    @Override
    public void showInfo(String info) {
        Toast.makeText(getActivity().getApplicationContext(), info, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showEmptyUsernameError(boolean show) {
        mBinding.usernameEdit.setError(show ? getString(R.string.error_username_empty) : null);
    }

    @Override
    public void showEmptyPasswordError(boolean show) {
        mBinding.passwordEdit.setError(show ? getString(R.string.error_password_empty) : null);
    }

    @Override
    public void onPause() {
        super.onPause();
        mPresenter.onPause();
        dismiss();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.setView(null);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mActivityCallback = (HomeActivityCallback) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivityCallback = null;
    }
}
