package com.testtowapi.gpstracker.ui.auth;

import com.testtowapi.gpstracker.model.auth.Credentials;
import com.testtowapi.gpstracker.ui.BaseView;

/**
 * Created by Think on 19/09/2016.
 */

public interface AuthView extends BaseView {
    void setSavedCredentials(Credentials credentials);

    void finish();

    void logInSuccess();

    void showInfo(String info);

    void showEmptyUsernameError(boolean show);



    void showEmptyPasswordError(boolean show);


}
