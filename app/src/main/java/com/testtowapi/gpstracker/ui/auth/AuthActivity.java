package com.testtowapi.gpstracker.ui.auth;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.testtowapi.gpstracker.App;
import com.testtowapi.gpstracker.R;
import com.testtowapi.gpstracker.databinding.ComponentAuthBinding;
import com.testtowapi.gpstracker.model.auth.Credentials;
import com.testtowapi.gpstracker.ui.auth.di.AuthModule;
import com.testtowapi.gpstracker.ui.home.HomeActivity;

import javax.inject.Inject;

public class AuthActivity extends AppCompatActivity implements AuthView {
    @Inject AuthPresenter mPresenter;
    private ComponentAuthBinding mBinding;

    public static Intent createIntent(Context context) {
        Intent intent = new Intent(context, AuthActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((App) getApplication()).getAppComponent().sub(new AuthModule(this)).inject(this);
        mBinding = DataBindingUtil.setContentView(this, R.layout.component_auth);
        setSupportActionBar(mBinding.toolbar);
        getSupportActionBar().setTitle("Login");
        mBinding.content.setEventHandler(mPresenter);
        mPresenter.setView(this);
        mPresenter.onCreateView();
    }

    @Override
    public void setSavedCredentials(Credentials savedCredentials) {
        mBinding.content.setCredentials(savedCredentials);
    }

    @Override
    public void logInSuccess() {
        Toast.makeText(this, "Login success", Toast.LENGTH_SHORT).show();
        startActivity(HomeActivity.createIntent(this));
    }

    @Override
    public void showInfo(String info) {
        Toast.makeText(this, info, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showEmptyUsernameError(boolean show) {
        mBinding.content.usernameEdit.setError(show ? getString(R.string.error_username_empty) : null);
    }

    @Override
    public void showEmptyPasswordError(boolean show) {
        mBinding.content.passwordEdit.setError(show ? getString(R.string.error_password_empty) : null);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mPresenter.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.setView(null);
    }
}
