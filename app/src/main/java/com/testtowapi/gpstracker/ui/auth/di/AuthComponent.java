package com.testtowapi.gpstracker.ui.auth.di;

import com.testtowapi.gpstracker.ui.auth.AuthActivity;
import com.testtowapi.gpstracker.ui.auth.AuthDialog;

import dagger.Subcomponent;

@PerAuth
@Subcomponent(modules = AuthModule.class)
public interface AuthComponent {
    void inject(AuthActivity injectee);

    void inject(AuthDialog authDialog);
}


