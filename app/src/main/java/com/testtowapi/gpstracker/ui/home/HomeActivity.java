package com.testtowapi.gpstracker.ui.home;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v13.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.CompoundButton;

import com.testtowapi.gpstracker.App;
import com.testtowapi.gpstracker.R;
import com.testtowapi.gpstracker.databinding.ActivityHomeBinding;
import com.testtowapi.gpstracker.ui.auth.AuthDialog;
import com.testtowapi.gpstracker.ui.auth.HomeActivityCallback;
import com.testtowapi.gpstracker.ui.home.di.HomeModule;
import com.testtowapi.gpstracker.utils.ActivityUtils;

import javax.inject.Inject;

public class HomeActivity extends AppCompatActivity implements HomeView, HomeActivityCallback {

    public static final int ALL_PERMISSIONS = 156;
    @Inject HomePresenter mPresenter;
    private ActivityHomeBinding mBinding;
    private CompoundButton.OnCheckedChangeListener toggleCheckedChanged = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            mPresenter.onCheckedChanged(b);
        }
    };

    public static Intent createIntent(Context context) {
        Intent intent = new Intent(context, HomeActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityUtils.lockOrientation(this);
        ((App) getApplication()).getAppComponent().sub(new HomeModule(this)).inject(this);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_home);
        mPresenter.setView(this);
        mBinding.setEventHandler(mPresenter);
        mBinding.toggleLocationStatus.setOnCheckedChangeListener(toggleCheckedChanged);
        setSupportActionBar(mBinding.toolbar);
        showAuthDialog();
    }

    private void showAuthDialog() {
        new AuthDialog().show(getFragmentManager(), "AUTH_DIALOG");
    }

    @Override
    public void loginSuccess() {
        startActivityOnAuthSuccess();

    }

    private void startActivityOnAuthSuccess() {
        if (hasPermissions(this, getPermissions())) {
            mPresenter.onCreateView();
        }
        else ActivityCompat.requestPermissions(this, getPermissions(), ALL_PERMISSIONS);
    }

    private boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    private String[] getPermissions() {
        return new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION};
    }

    @Override
    public void cancel() {
        finish();
    }

    @Override
    public void showLocationServiceStatus(boolean isStarted) {
        mBinding.toggleLocationStatus.setOnCheckedChangeListener(null);
        mBinding.toggleLocationStatus.setChecked(isStarted);
        mBinding.toggleLocationStatus.setOnCheckedChangeListener(toggleCheckedChanged);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mPresenter.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.onResume();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == ALL_PERMISSIONS) {
            if (grantResults.length > 0) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mPresenter.onCreateView();
                }
            }
        }
    }

}

