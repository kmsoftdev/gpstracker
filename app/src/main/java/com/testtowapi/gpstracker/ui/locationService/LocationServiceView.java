package com.testtowapi.gpstracker.ui.locationService;

import com.testtowapi.gpstracker.ui.BaseView;

/**
 * Created by Think on 20/09/2016.
 */

public interface LocationServiceView extends BaseView {
    void stopSelf();

    void displaySelf();

    void killSelf();
}
