package com.testtowapi.gpstracker.ui.auth;

/**
 * Created by Think on 20/09/2016.
 */
public interface HomeActivityCallback {
    void loginSuccess();
    void cancel();
}
