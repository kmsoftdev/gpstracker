package com.testtowapi.gpstracker.business.auth;

import android.content.SharedPreferences;

import com.testtowapi.gpstracker.model.auth.AuthService;
import com.testtowapi.gpstracker.model.auth.Credentials;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Observable;

/**
 * Created by Think on 19/09/2016.
 */
@Singleton
public class AuthController {
    private static final String KEY_SAVED_USERNAME = "SAVED_USERNAME";
    private static final String KEY_SAVED_PASSWORD = "SAVED_PASSWORD";
    private final SharedPreferences mSharedPrefs;
    private final SessionManager mSessionManager;
    private final AuthService mAuthService;

    @Inject
    public AuthController(SharedPreferences sharedPreferences, SessionManager mSessionManager, AuthService mAuthService) {

        mSharedPrefs = sharedPreferences;
        this.mSessionManager = mSessionManager;
        this.mAuthService = mAuthService;
    }

    public Observable<Credentials> getSavedCredentials() {
        return Observable.defer(() -> {
            String savedUsername = mSharedPrefs.getString(KEY_SAVED_USERNAME, "");
            String savedPassword = mSharedPrefs.getString(KEY_SAVED_PASSWORD, "");
            return Observable.just(new Credentials(savedUsername, savedPassword));
        });
    }

    public Observable<Boolean> logIn(Credentials credentials) {
        return mAuthService.login(credentials.getUsername(),
                                  credentials.getPassword(),
                                  0,
                                  null,
                                  "json")
                           .flatMap(authResponse -> {
                               if (authResponse != null && authResponse.getResult().isS()) {
                                   return Observable.just(authResponse.getAuthKey());
                               }
                               else
                                   return Observable.error(new Throwable(authResponse.getResult().getD()));
                           })
                           .doOnNext(authKey -> {
                               mSessionManager.storeAuthKey(authKey);
                               saveCredentials(credentials);
                           })
                           .map(s -> true);
    }

    private void saveCredentials(Credentials credentials) {
        SharedPreferences.Editor edit = mSharedPrefs.edit();
        edit.putString(KEY_SAVED_USERNAME, credentials.getUsername());
        edit.putString(KEY_SAVED_PASSWORD, credentials.getPassword());
        edit.apply();
    }

}
