package com.testtowapi.gpstracker.business.location;

import android.location.Location;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Observable;

/**
 * Created by Think on 20/09/2016.
 */
@Singleton
public class LocationHelper {

    @Inject
    public LocationHelper() {
    }

    public Observable<Boolean> moreThan500m(Location previousLocation, Location currentLocation) {
        return Observable.defer(() -> {
            if (previousLocation == null || currentLocation == null) return Observable.just(false);
            return Observable.just(currentLocation.distanceTo(previousLocation) > 500);
        });
    }

}
