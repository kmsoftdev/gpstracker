package com.testtowapi.gpstracker.business;

import android.util.Log;

import com.jakewharton.rxrelay.BehaviorRelay;
import com.testtowapi.gpstracker.business.bus.locationServiceStatus.LocationStatusEventContext;
import com.testtowapi.gpstracker.model.bus.locationStatus.LocationServiceStatusChanged;
import com.testtowapi.gpstracker.model.bus.locationStatus.LocationStatusEvent;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Subscription;

@Singleton
public class Bus {

    private BehaviorRelay<LocationStatusEvent> mLocationServiceStatusPublisher;

    @Inject
    public Bus() {
        this.mLocationServiceStatusPublisher = BehaviorRelay.create();
    }

    public void publishLocationStatusChangedEvent(boolean isStarted) {
        mLocationServiceStatusPublisher.call(new LocationServiceStatusChanged(isStarted));
    }

    public Subscription subscribeToBusEvents(LocationStatusEventContext context) {
        return mLocationServiceStatusPublisher.subscribe(busEvent -> {
            busEvent.execute(context);
        }, throwable -> {
            Log.d("kmtag", "ignore: ");
        });
    }

}
