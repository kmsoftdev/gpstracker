package com.testtowapi.gpstracker.business.location;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.testtowapi.gpstracker.business.Bus;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by Think on 20/09/2016.
 */
@Singleton
public class LocationReceiver implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private final Bus mBus;
    private final GoogleApiClient mApiClient;
    private final LocationRequest mLocationRequest;
    private final LocationHandler mLocationHandler;

    @Inject
    public LocationReceiver(Bus bus,
                            GoogleApiClient apiClient,
                            LocationRequest locationRequest,
                            LocationHandler locationHandler) {
        mBus = bus;
        mApiClient = apiClient;
        mLocationRequest = locationRequest;
        mLocationHandler = locationHandler;
    }

    public void onResume() {
        mApiClient.registerConnectionCallbacks(this);
        mApiClient.registerConnectionFailedListener(this);
        mApiClient.connect();
    }

    public void onPause() {
        mLocationHandler.onPause();
        mApiClient.unregisterConnectionCallbacks(this);
        mApiClient.unregisterConnectionFailedListener(this);
        mApiClient.disconnect();

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(mApiClient.getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mApiClient.getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mLocationHandler.onResume();
        mBus.publishLocationStatusChangedEvent(true);
        LocationServices.FusedLocationApi.requestLocationUpdates(mApiClient, mLocationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {
        mLocationHandler.onPause();
        mBus.publishLocationStatusChangedEvent(false);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        mLocationHandler.onPause();
        mBus.publishLocationStatusChangedEvent(false);
    }

    @Override
    public void onLocationChanged(Location location) {
        mLocationHandler.locationChanged(location);
    }
}
