package com.testtowapi.gpstracker.business.location;

import android.location.Location;
import android.util.Log;

import com.testtowapi.gpstracker.business.auth.SessionManager;
import com.testtowapi.gpstracker.model.location.LocationUpdate;
import com.testtowapi.gpstracker.model.location.LocationUpdateService;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Singleton;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Think on 20/09/2016.
 */
@Singleton
public class LocationHandler {
    private final LocationUpdateService mLocationUpdateService;
    private final LocationHelper mLocationHelper;
    private final SessionManager mSessionManager;
    private final CompositeSubscription subs;
    private Location mCurrentLocation;
    private Location mLastSavedLocation;

    @Inject
    public LocationHandler(LocationUpdateService locationUpdateService,
                           LocationHelper locationHelper,
                           SessionManager sessionManager) {
        mLocationUpdateService = locationUpdateService;
        mLocationHelper = locationHelper;
        mSessionManager = sessionManager;
        subs = new CompositeSubscription();
    }

    /**
     * called by parent (LocationReceiver)
     */
    public void locationChanged(Location newLocation) {
        mCurrentLocation = newLocation;
        subs.add(mLocationHelper.moreThan500m(mLastSavedLocation, mCurrentLocation)
                                .filter(Boolean::booleanValue)
                                .flatMap(isMoreThan500m -> saveLocation(mCurrentLocation))
                                .subscribeOn(Schedulers.io())
                                .subscribe(ignored -> {
                                    mLastSavedLocation = mCurrentLocation;
                                    Log.d("kmtag", "updatedEvery500mLocation: ");
                                }, throwable -> {
                                    Log.d("kmtag", "error occured while processing new location");
                                }));
    }

    /**
     * Execute remote server call
     */
    private Observable<String> saveLocation(Location currentLocation) {
        return Observable.defer(() -> {
            Call<ResponseBody> call = mLocationUpdateService
                    .postLocationUpdate(LocationUpdate.fromLocation(currentLocation),
                                        mSessionManager.getAuthTokenSync());
            try {
                Response<ResponseBody> execute = call.execute();
                return Observable.just(execute.body().string());
            } catch (IOException e) {
                e.printStackTrace();
                return Observable.error(e);
            }
        });
    }

    /**
     * called by parent
     */
    public void onResume() {
        subs.addAll(registerIntervalServerUpdates());
    }

    /**
     * interval(initial delay, interval, timeUnit[TimeUnit.Seconds/TimeUnit.Minutes/Hours etc)
     */
    private Subscription registerIntervalServerUpdates() {
        return Observable.interval(1, 1, TimeUnit.MINUTES)
                         .flatMap(tick -> saveLocation(mCurrentLocation))
                         .subscribeOn(Schedulers.io())
                         .observeOn(AndroidSchedulers.mainThread())
                         .subscribe(ignoredResponse -> {
                             Log.d("kmtag", "saved event on the server: ");
                             mLastSavedLocation = mCurrentLocation;
                         }, throwable -> {
                             Log.d("kmtag", "error occured while saving location: ");
                         });
    }

    /**
     * called by parent
     */
    public void onPause() {
        subs.clear();
    }
}
