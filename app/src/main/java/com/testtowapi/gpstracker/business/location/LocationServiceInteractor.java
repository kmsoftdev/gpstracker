package com.testtowapi.gpstracker.business.location;

import android.content.Context;

import com.testtowapi.gpstracker.ui.locationService.LocationService;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class LocationServiceInteractor {

    private final Context mContext;

    @Inject
    public LocationServiceInteractor(Context context) {
        mContext = context;
    }

    public void startLocationService(boolean start) {
        mContext.startService(LocationService.createIntent(mContext, start));
    }
}
