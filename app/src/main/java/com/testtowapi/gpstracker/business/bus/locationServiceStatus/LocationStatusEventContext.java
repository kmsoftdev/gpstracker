package com.testtowapi.gpstracker.business.bus.locationServiceStatus;

public interface LocationStatusEventContext {
    void locationServiceStatusChanged(boolean mIsStarted);
}
