package com.testtowapi.gpstracker.business.auth;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Observable;

/**
 * Created by Think on 20/09/2016.
 */
@Singleton
public class SessionManager {

    private String mAuthKey;

    @Inject
    public SessionManager() {

    }

    public void storeAuthKey(String authKey) {
        this.mAuthKey = authKey;
    }

    public Observable<String> getAuthToken() {
        return Observable.just(mAuthKey);
    }

    public String getAuthTokenSync() {
        return mAuthKey;
    }
}
