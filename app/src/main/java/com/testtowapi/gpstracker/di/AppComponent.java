package com.testtowapi.gpstracker.di;

import com.testtowapi.gpstracker.ui.auth.di.AuthComponent;
import com.testtowapi.gpstracker.ui.auth.di.AuthModule;
import com.testtowapi.gpstracker.ui.home.di.HomeComponent;
import com.testtowapi.gpstracker.ui.home.di.HomeModule;
import com.testtowapi.gpstracker.ui.locationService.di.LocationServiceComponent;
import com.testtowapi.gpstracker.ui.locationService.di.LocationServiceModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, RemoteModule.class, LocationModule.class})
public interface AppComponent {

    AuthComponent sub(AuthModule authModule);

    HomeComponent sub(HomeModule homeModule);

    LocationServiceComponent sub(LocationServiceModule locationServiceModule);
}

