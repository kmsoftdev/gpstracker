package com.testtowapi.gpstracker.di;

import android.content.Context;
import android.content.SharedPreferences;

import com.testtowapi.gpstracker.App;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {
    private static final String KEY_AUTH_PREFS = "AUTH_PREFS";
    private App mApp;

    public AppModule(App mApp) {
        this.mApp = mApp;
    }

    @Provides
    @Singleton
    Context provideContext() {
        return this.mApp;
    }

    @Provides
    @Singleton
    SharedPreferences getSharedPrefs(Context context) {
        return context.getSharedPreferences(KEY_AUTH_PREFS, Context.MODE_PRIVATE);
    }

}

