package com.testtowapi.gpstracker.di;

import android.content.Context;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.testtowapi.gpstracker.business.Bus;
import com.testtowapi.gpstracker.business.auth.SessionManager;
import com.testtowapi.gpstracker.business.location.LocationHandler;
import com.testtowapi.gpstracker.business.location.LocationHelper;
import com.testtowapi.gpstracker.model.location.LocationUpdateService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class LocationModule {

    @Singleton
    @Provides
    GoogleApiClient provideClient(Context context) {
        return new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API)
                .build();
    }

    @Singleton
    @Provides
    LocationRequest provideFineLocationRequest() {
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        return mLocationRequest;
    }

    @Provides
    @Singleton
    LocationUpdateService provideLocationUpdateService(Retrofit retrofit) {
        return retrofit.create(LocationUpdateService.class);
    }



}
