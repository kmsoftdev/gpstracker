package com.testtowapi.gpstracker.model.bus.locationStatus;

import com.testtowapi.gpstracker.business.bus.locationServiceStatus.LocationStatusEventContext;

/**
 * Created by Think on 20/09/2016.
 */

public abstract class LocationStatusEvent {
    public abstract void execute(LocationStatusEventContext context);

}
