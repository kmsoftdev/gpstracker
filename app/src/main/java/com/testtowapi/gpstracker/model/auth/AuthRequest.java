package com.testtowapi.gpstracker.model.auth;

public class AuthRequest {
    private String U;
    private String P;
    private int OS = 0;
    private String format = "json";

    public AuthRequest(Credentials credentials) {
        this.U = credentials.getUsername();
        this.P = credentials.getPassword();
    }

    public static AuthRequest withCredentials(Credentials credentials) {
        return new AuthRequest(credentials);
    }

    public int getOS() {
        return OS;
    }

    public void setOS(int OS) {
        this.OS = OS;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getP() {
        return P;
    }

    public void setP(String p) {
        P = p;
    }

    public String getU() {
        return U;
    }

    public void setU(String u) {
        U = u;
    }
}
