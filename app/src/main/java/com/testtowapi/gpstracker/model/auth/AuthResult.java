package com.testtowapi.gpstracker.model.auth;

/**
 * Created by Think on 20/09/2016.
 */
public class AuthResult {
    boolean S;

    public String getD() {
        return D;
    }

    public void setD(String d) {
        D = d;
    }

    public boolean isS() {
        return S;
    }

    public void setS(boolean s) {
        S = s;
    }

    String D;
}
