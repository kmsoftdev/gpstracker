package com.testtowapi.gpstracker.model.location;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by Think on 20/09/2016.
 */

public interface LocationUpdateService {

    @POST("/driver/gps")
    Call<ResponseBody> postLocationUpdate(@Body LocationUpdate locationUpdate, @Header("Authorization") String authHeader);
}
