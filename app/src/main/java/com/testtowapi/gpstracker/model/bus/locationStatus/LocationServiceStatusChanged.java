package com.testtowapi.gpstracker.model.bus.locationStatus;

import com.testtowapi.gpstracker.business.bus.locationServiceStatus.LocationStatusEventContext;

/**
 * Created by Think on 20/09/2016.
 */

public class LocationServiceStatusChanged extends LocationStatusEvent {
    private final boolean mIsStarted;

    public LocationServiceStatusChanged(boolean isStarted) {
        this.mIsStarted = isStarted;
    }

    @Override
    public void execute(LocationStatusEventContext eventContext) {
        eventContext.locationServiceStatusChanged(mIsStarted);
    }
}
