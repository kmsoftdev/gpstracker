package com.testtowapi.gpstracker.model.location;

import android.location.Location;

/**
 * Created by Think on 28/09/2016.
 */
public class LocationUpdate {
    private double LT;
    private double LG;
    private int SG;
    private int SP;
    private int AL;
    private int DI;

    public LocationUpdate() {
    }

    public LocationUpdate(double LT, double LG, int SG, int SP, int AL, int DI) {
        this.LT = LT;
        this.LG = LG;
        this.SG = SG;
        this.SP = SP;
        this.AL = AL;
        this.DI = DI;
    }

    public static LocationUpdate fromLocation(Location location) {
        if (location != null) {
            return new LocationUpdate(location.getLatitude(),
                                      location.getLongitude(),
                                      -1,
                                      (int) location.getSpeed(),
                                      (int) location.getAltitude(),
                                      (int) location.getBearing()
            );
        }
        else return new LocationUpdate();
    }

    public double getLT() {
        return LT;
    }

    public void setLT(double LT) {
        this.LT = LT;
    }

    public double getLG() {
        return LG;
    }

    public void setLG(double LG) {
        this.LG = LG;
    }

    public int getSG() {
        return SG;
    }

    public void setSG(int SG) {
        this.SG = SG;
    }

    public int getSP() {
        return SP;
    }

    public void setSP(int SP) {
        this.SP = SP;
    }

    public int getAL() {
        return AL;
    }

    public void setAL(int AL) {
        this.AL = AL;
    }

    public int getDI() {
        return DI;
    }

    public void setDI(int DI) {
        this.DI = DI;
    }
}
