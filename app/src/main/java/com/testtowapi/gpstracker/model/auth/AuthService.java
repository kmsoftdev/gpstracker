package com.testtowapi.gpstracker.model.auth;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface AuthService {

    @GET("/driver/login")
    Observable<AuthResponse> login(@Query("U") String username,
                                   @Query("P") String password,
                                   @Query("OS") int os,
                                   @Query("NI") String push,
                                   @Query("format") String format);
}
