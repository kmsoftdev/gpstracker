package com.testtowapi.gpstracker.model.auth;

/**
 * Created by Think on 20/09/2016.
 */
public class AuthResponse {
    public String getAuthKey() {
        return AuthKey;
    }

    public void setAuthKey(String authKey) {
        AuthKey = authKey;
    }

    public AuthResult getResult() {
        return Result;
    }

    public void setResult(AuthResult result) {
        Result = result;
    }

    String AuthKey;
    AuthResult Result;
}
